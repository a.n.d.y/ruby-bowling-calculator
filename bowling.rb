require_relative 'frame'
require_relative 'game'

game = Game.new

frame_count = 0

while frame_count < 10 do
#while true do
  frame_count += 1
  
  frame = Frame.new
  frame.frame_number = frame_count
  frame.game = game
  
  while !frame.is_finished? do
    puts('')
    puts('')
    puts('Frame-Number: '+frame.frame_number.to_s)
    puts('Ball: '+frame.ball.to_s)
    puts('')
    print('Type in Number of Pins: ')
    pins = gets.to_s.chomp.to_i
    
    #validate input
    if ([1,3].include?(frame.ball) && pins > 10) || (!frame.is_last_frame? && frame.ball == 2 && (frame.ball1.to_i + pins) > 10)
      #more than 10 pins in one frame
      puts('---------------------------------------------------------')
      puts("!!!!!! #{pins} is a invalid number of fallen Pins!!!!!!")
      puts('---------------------------------------------------------')
      redo
    end	    
    
    frame.send("ball#{frame.ball.to_s}=", pins)
  end
  
  game.frames << frame
  puts('')
  puts('-------------------------------')
  game.frames.each do |f|
    puts("Frame-Nr:#{f.frame_number.to_s} (Pins: #{f.ball1_display_value} | #{f.ball2_display_value} #{f.is_last_frame? ? ('| ' + f.ball3_display_value) : ''}) => "+ game.result_of_frame(f).to_s)
  end
  puts('-------------------------------')
  puts('')

end