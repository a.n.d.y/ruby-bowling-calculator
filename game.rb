class Game
  
  #maximal number of frames
  MAX_FRAMES_PER_GAME = 10.freeze
  
  attr_accessor :frames
  
  def initialize
    @frames ||= []
  end
  
  def frames
    @frames
  end

  def current_frame_number
    @frames.last.frame_number.to_i rescue nil
  end
  
  def current_frame_finished?
    @frames.last.is_finished? rescue false
  end
  
  def game_finished?
    @frames.last.frame_number == MAX_FRAMES_PER_GAME rescue false
  end
  
  def result_of_frame(frame)
    #loops recursively through all previous frames of a specific frame 
    #and calculates the REAL points of a frame
    
    fr = frame.result #fallen pins of the frame (default result, independed of strikes and spares)
    f_index = @frames.index(frame)
    next_frame = @frames[f_index+1]
    
    #check for strike or spare
    if(frame.is_strike? && next_frame)
      #points of next two balls will also be added to current frame
      if(next_frame.is_strike? && !next_frame.is_last_frame?)
	#if the next frame was also a strike
	fr += 10 #we add the 10 points (strike) of next frame
	fr += @frames[f_index+2].ball1.to_i unless @frames[f_index+2].nil? #and add the points of the first ball of over-next frame
      else
	#we add the points of the next frame also to current frame (adds also the result of the first two balls of last frame)
        fr += next_frame.ball1.to_i + next_frame.ball2.to_i
      end
    elsif(frame.is_spare? && next_frame)
      #points of first ball of the next frame will also be added to current frame
      fr += next_frame.ball1.to_i
    end
    
    #stop ree-looping if we have the first frame of the game!!
    fr += result_of_frame(@frames[f_index-1]) unless f_index == 0
    
    return fr
  end
  
end