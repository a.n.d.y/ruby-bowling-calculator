# Ruby Bowling Calculator

Ruby script which demonstrates the calculation of a Bowling Game.

You have to type in the fallen pins for each throw.
### Start it with:
```ruby
ruby bowling.rb
```