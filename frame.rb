class Frame
  
  #max three throws per frame
  attr_accessor :ball1, :ball2, :ball3

  #each frame has a uniqe number per game from 1 - Game::MAX_FRAMES_PER_GAME
  attr_accessor :frame_number

  #each frame depends to a game
  attr_accessor :game
  
  def result
    #default points of a frame (independend of strikes and spares)
    self.ball1.to_i+self.ball2.to_i+self.ball3.to_i
  end
  
  def is_strike?
    #if first ball has 10 points then it is a strike
    self.ball1.to_i == 10
  end
   
  def is_spare?
    #no strike but 10 points with first and second ball
    !self.is_strike? && (self.ball1.to_i+self.ball2.to_i == 10)
  end	  
  
  def is_normal?
    #two balls under 10 points
    !self.ball1.nil? && !self.ball2.nil? && (self.ball1.to_i+self.ball2.to_i < 10)
  end
  
  def is_finished?
    if self.is_last_frame?
      #we have a third ball if first or second ball was a strike or spare in last frame
      self.is_normal? || !self.ball3.nil?
    else
      self.is_strike? || self.is_spare? || self.is_normal?
    end
  end
  
  def is_last_frame?
    self.frame_number == Game::MAX_FRAMES_PER_GAME
  end
  
  def ball
    if self.ball1.nil?
      1
    elsif self.ball2.nil?
      2
    elsif self.ball3.nil?
      3
    end
  end
  
  def ball1_display_value
    if self.is_strike?
      'x'
    else
      self.ball1.to_s
    end
  end
  
  def ball2_display_value
    if self.is_spare?
      '/'
    elsif self.is_last_frame? && self.ball2.to_i == 10
      #in last frame the second ball could also be a strike
      'x'
    else
      self.ball2.to_s
    end
  end
 
  def ball3_display_value
    #only for last frame
    if self.ball3.to_i == 10
      'x'
    else
      self.ball3.to_s
    end
  end

end